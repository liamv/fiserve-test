﻿using System.Collections.Generic;

namespace FiservTest.Core
{
    public interface IPrizeDraw
    {
        void AddOrders(List<int> newOrders);
        int CalculatePrize();
        long GetTotalPrizes();
        int GetDrawSize();
    }
}

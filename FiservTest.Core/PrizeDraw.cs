﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FiservTest.Core
{
    public class PrizeDraw : IPrizeDraw
    {
        protected List<int> Orders = new List<int>();
        protected long TotalPrizes;

        public void AddOrders(List<int> newOrders)
        {
            if (newOrders == null)
            {
                throw new ArgumentNullException(nameof(newOrders));
            }

            if (newOrders.Min() < 1)
            {
                throw new ArgumentException("Orders must have a positive value", nameof(newOrders));
            }

            Orders.AddRange(newOrders);
        }

        public int CalculatePrize()
        {
            if (Orders.Count == 0) return 0;
            if (Orders.Count == 1) return Orders[0];

            /* single pass over */
            
            var maxValue = 0;
            var minValue = 0;
            var maxIndex = -1;
            var minIndex = -1;

            for (var i = 0; i < Orders.Count; i++)
            {
                var value = Orders[i];
                if (maxIndex == -1 || value > maxValue)
                {
                    maxValue = value;
                    maxIndex = i;
                }

                if (minIndex == -1 || value < minValue)
                {
                    minValue = value;
                    minIndex = i;
                }
            }

            Orders.RemoveAt(maxIndex);

            //if min and max are different, remove at the adjusted min index
            if (minIndex != maxIndex)
            {
                Orders.RemoveAt(minIndex > maxIndex ? minIndex - 1 : minIndex);
            }
            //if min and max are the same, find the first/next min value
            else
            {
                Orders.Remove(minValue);
            }

            var prize = maxValue - minValue;
            TotalPrizes += prize;
            return prize;
        }

        public long GetTotalPrizes() => TotalPrizes;
        public int GetDrawSize() => Orders.Count;
    }
}

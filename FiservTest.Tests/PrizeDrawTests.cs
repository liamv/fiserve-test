﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using FiservTest.Core;

namespace FiservTest.Tests
{
    [TestClass]
    public class PrizeDrawTests
    {
        protected int MaximumNumberOfDays = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfDays"]);
        protected int MaximumOrderValue = int.Parse(ConfigurationManager.AppSettings["MaximumOrderValue"]);
        protected int MaximumNumberOfOrdersPerDay = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrdersPerDay"]);
        protected int MaximumNumberOfOrders = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrders"]);

        public IPrizeDraw CreatePrizeDraw()
        {
            return new PrizeDraw();
        }

        [TestMethod]
        public void NoOrders()
        {
            var draw = CreatePrizeDraw();
            Assert.AreEqual(0, draw.CalculatePrize());
        }

        [TestMethod]
        public void AddOrdersNull()
        {
            var draw = CreatePrizeDraw();
            Assert.ThrowsException<ArgumentNullException>(() => { draw.AddOrders(null); });
        }

        [TestMethod]
        public void AddOrdersNegative()
        {
            var draw = CreatePrizeDraw();
            Assert.ThrowsException<ArgumentException>(() => { draw.AddOrders(new List<int>{ -1 }); });
        }

        [TestMethod]
        public void EmptyOrders()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int>());
            Assert.AreEqual(0, draw.CalculatePrize());
        }

        [TestMethod]
        public void SubsequentPrizes()
        {
            var draw = CreatePrizeDraw();
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(0, draw.CalculatePrize());
        }

        [TestMethod]
        public void OrdersAdded()
        {
            var draw = CreatePrizeDraw();
            Assert.AreEqual(0, draw.GetDrawSize());
            draw.AddOrders(new List<int> { 1, 5, 10, 15 });
            Assert.AreEqual(4, draw.GetDrawSize());
            draw.AddOrders(new List<int> { 1, 5, 5, 10});
            Assert.AreEqual(8, draw.GetDrawSize());
        }

        [TestMethod]
        public void OrdersRemoved()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 1, 5, 10, 15 });
            Assert.AreEqual(4, draw.GetDrawSize());
            Assert.AreEqual(14, draw.CalculatePrize());
            Assert.AreEqual(2, draw.GetDrawSize());
            Assert.AreEqual(5, draw.CalculatePrize());
            Assert.AreEqual(0, draw.GetDrawSize());
        }

        [TestMethod]
        public void TwoDuplicateValues()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 5, 5 });
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(0, draw.GetDrawSize());
        }

        [TestMethod]
        public void DuplicateValues()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 10, 10, 10, 10 });
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(2, draw.GetDrawSize());
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(0, draw.GetDrawSize());
        }

        [TestMethod]
        public void IncreasingDoubleValues()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 5, 5, 10, 10, 15, 15 });
            Assert.AreEqual(10, draw.CalculatePrize());
            Assert.AreEqual(4, draw.GetDrawSize());
            Assert.AreEqual(10, draw.CalculatePrize());
            Assert.AreEqual(2, draw.GetDrawSize());
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(0, draw.GetDrawSize());
        }

        [TestMethod]
        public void DecreasingDoubleValues()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 15, 15, 10, 10, 5, 5 });
            Assert.AreEqual(10, draw.CalculatePrize());
            Assert.AreEqual(4, draw.GetDrawSize());
            Assert.AreEqual(10, draw.CalculatePrize());
            Assert.AreEqual(2, draw.GetDrawSize());
            Assert.AreEqual(0, draw.CalculatePrize());
            Assert.AreEqual(0, draw.GetDrawSize());
        }

        [TestMethod]
        public void SuppliedExample()
        {
            var draw = CreatePrizeDraw();
            draw.AddOrders(new List<int> { 1, 2, 3 });
            Assert.AreEqual(2, draw.CalculatePrize(), "First Prize");
            draw.AddOrders(new List<int> { 1, 1 });
            Assert.AreEqual(1, draw.CalculatePrize(), "Second Prize");
            draw.AddOrders(new List<int> { 10, 5, 5, 1 });
            Assert.AreEqual(9, draw.CalculatePrize(), "Third Prize");
            draw.AddOrders(new List<int>());
            Assert.AreEqual(4, draw.CalculatePrize(), "Fourth Prize");
            draw.AddOrders(new List<int> { 2 });
            Assert.AreEqual(3, draw.CalculatePrize(), "Five Prize");
            Assert.AreEqual(19, draw.GetTotalPrizes(), "Total Prizes");
        }

        [TestMethod]
        public void MaximumOrders()
        {
            var rand = new Random();

            var draw = CreatePrizeDraw();
            for (var i = 0; i < 10; i++)
            {
                var orders = new List<int> { 1 };
                for (var x = 0; x < MaximumNumberOfOrdersPerDay - 1; x++)
                {
                    orders.Add(rand.Next(MaximumOrderValue));
                }
                draw.AddOrders(orders);
                draw.CalculatePrize();
            }
        }

        [TestMethod]
        public void MaximumDays()
        {
            var rand = new Random();
            var numberOfOrdersPerDay = (int)Math.Floor((decimal)MaximumNumberOfOrders / MaximumNumberOfDays);

            var draw = CreatePrizeDraw();
            for (var i = 0; i < MaximumNumberOfDays; i++)
            {
                var orders = new List<int>();
                for (var x = 0; x < rand.Next(numberOfOrdersPerDay); x++)
                {
                    orders.Add(rand.Next(MaximumOrderValue));
                }
                draw.AddOrders(orders);
                draw.CalculatePrize();
            }
        }

        [TestMethod]
        public void MaximumTotalPrizes()
        {
            var numberOfOrdersPerDay = (int)Math.Floor((decimal)MaximumNumberOfOrders / MaximumNumberOfDays);

            var draw = CreatePrizeDraw();
            for (var i = 0; i < MaximumNumberOfDays; i++)
            {
                var orders = new List<int>();
                for (var x = 0; x < numberOfOrdersPerDay; x++)
                {
                    orders.Add(MaximumOrderValue);
                }
                draw.AddOrders(orders);
                draw.CalculatePrize();
            }

            draw.GetTotalPrizes();
        }
    }
}

﻿using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.Linq;
using FiservTest.Ui;

namespace FiservTest.Tests
{
    [TestClass]
    public class PrizeDrawUiTests
    {
        protected int MaximumNumberOfDays = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfDays"]);
        protected int MaximumOrderValue = int.Parse(ConfigurationManager.AppSettings["MaximumOrderValue"]);
        protected int MaximumNumberOfOrdersPerDay = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrdersPerDay"]);
        protected int MaximumNumberOfOrders = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrders"]);

        public IPrizeDrawUi CreatePrizeDrawUi()
        {
            return new PrizeDrawUi(new Stubs.Console());
        }

        [TestMethod]
        public void DaysCountInputInvalid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseNumberOfDaysInput("a"), "Letter");
            Assert.IsNull(ui.ParseNumberOfDaysInput("?"), "Symbol");
            Assert.IsNull(ui.ParseNumberOfDaysInput("1.1"), "Decimal");
            Assert.IsNull(ui.ParseNumberOfDaysInput("5 5"), "Spaces");
        }

        [TestMethod]
        public void DaysCountInputLowerBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseNumberOfDaysInput("-1"), "Negative");
            Assert.IsNull(ui.ParseNumberOfDaysInput("0"), "Zero");
            Assert.AreEqual(1, ui.ParseNumberOfDaysInput("1"));
        }

        [TestMethod]
        public void DaysCountInputUpperBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(MaximumNumberOfDays, ui.ParseNumberOfDaysInput(MaximumNumberOfDays.ToString()));
            Assert.IsNull(ui.ParseNumberOfDaysInput((MaximumNumberOfDays + 1).ToString()));
        }

        [TestMethod]
        public void DaysCountInputValid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(1, ui.ParseNumberOfDaysInput("1"));
            Assert.AreEqual(5, ui.ParseNumberOfDaysInput("5"));
            Assert.AreEqual(10, ui.ParseNumberOfDaysInput("10"));
        }

        [TestMethod]
        public void OrdersCountInputInvalid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineOrderCountInput("a"), "Letter");
            Assert.IsNull(ui.ParseDayLineOrderCountInput("?"), "Symbol");
            Assert.IsNull(ui.ParseDayLineOrderCountInput("1.1"), "Decimal");
        }

        [TestMethod]
        public void OrdersCountInputValid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(1, ui.ParseDayLineOrderCountInput("1"));
            Assert.AreEqual(2, ui.ParseDayLineOrderCountInput("2"));
            Assert.AreEqual(10, ui.ParseDayLineOrderCountInput("10"));
        }

        [TestMethod]
        public void OrdersCountInputBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineOrderCountInput("-1"), "Negative");
            Assert.AreEqual(0, ui.ParseDayLineOrderCountInput("0"), "Zero");
            Assert.AreEqual(MaximumNumberOfOrdersPerDay, ui.ParseDayLineOrderCountInput(MaximumNumberOfOrdersPerDay.ToString()), "Maximum");
            Assert.IsNull(ui.ParseDayLineOrderCountInput((MaximumNumberOfOrdersPerDay + 1).ToString()), "Too many");
        }

        [TestMethod]
        public void OrderValuesInputValid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(1, ui.ParseDayLineOrderValuesInput(new List<string> { "1" }).Sum());
            Assert.AreEqual(7, ui.ParseDayLineOrderValuesInput(new List<string> { "3", "4" }).Sum());
            Assert.AreEqual(12, ui.ParseDayLineOrderValuesInput(new List<string> { "3", "4", "5" }).Sum());
            Assert.AreEqual(55, ui.ParseDayLineOrderValuesInput(new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }).Sum());
        }

        [TestMethod]
        public void OrderValuesInputInvalid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "a", "6" }), "Letter (First");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "?", "4" }), "Symbol (First");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "1.1", "4" }), "Decimal (First)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "6", "a" }), "Letter (Last)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "3", "?" }), "Symbol (Last)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "4", "1.1" }), "Decimal (Last)");
        }

        [TestMethod]
        public void OrderValuesInputLowerBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "-1" }), "Negative (First)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "0" }), "Zero (First)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "1", "-1" }), "Negative (Last)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "1", "0" }), "Zero (Last)");
        }

        [TestMethod]
        public void OrderValuesInputUpperBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(MaximumOrderValue + 1, ui.ParseDayLineOrderValuesInput(new List<string> { "1", MaximumOrderValue.ToString() }).Sum(), "Maximum (First)");
            Assert.AreEqual(MaximumOrderValue + 1, ui.ParseDayLineOrderValuesInput(new List<string> { MaximumOrderValue.ToString(), "1" }).Sum(), "Maximum (Last)");

            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { (MaximumOrderValue + 1).ToString(), "1" }), "Too high (First)");
            Assert.IsNull(ui.ParseDayLineOrderValuesInput(new List<string> { "1", (MaximumOrderValue + 1).ToString() }), "Too high (Last)");
        }

        [TestMethod]
        public void DayLineInputValid()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(1, ui.ParseDayLineInput("1 1").Sum());
            Assert.AreEqual(7, ui.ParseDayLineInput("2 3 4").Sum());
            Assert.AreEqual(12, ui.ParseDayLineInput("3 3 4 5").Sum());
            Assert.AreEqual(55, ui.ParseDayLineInput("10 1 2 3 4 5 6 7 8 9 10").Sum());
        }

        [TestMethod]
        public void DayLineInputZero()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(0, ui.ParseDayLineInput("0").Count);
        }

        [TestMethod]
        public void DayLineInputCountMismatch()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineInput("5"), "None");
            Assert.IsNull(ui.ParseDayLineInput("5 1 2 3 4"), "Too Few");
            Assert.IsNull(ui.ParseDayLineInput("5 1 2 3 4 5 6"), "Too Many");
            Assert.IsNull(ui.ParseDayLineInput("0 1"), "Zero but included");
        }

        [TestMethod]
        public void DayLineMaximumOrdersPerDay()
        {
            var numberPerDay = MaximumNumberOfOrdersPerDay;
            var ui = CreatePrizeDrawUi();

            var input = new StringBuilder();
            input.Append(numberPerDay);
            for (var i = 0; i < numberPerDay; i++)
            {
                input.Append(" " + 1);
            }

            Assert.AreEqual(numberPerDay, ui.ParseDayLineInput(input.ToString()).Count);
        }

        [TestMethod]
        public void DayLineOrdersPerDayBounds()
        {
            var numberPerDay = MaximumNumberOfOrdersPerDay + 1;
            var ui = CreatePrizeDrawUi();

            var input = new StringBuilder();
            input.Append(numberPerDay);
            for (var i = 0; i < numberPerDay; i++)
            {
                input.Append(" " + 1);
            }

            Assert.IsNull(ui.ParseDayLineInput(input.ToString()));
        }

        [TestMethod]
        public void MaximumTotalOrders()
        {
            var ui = CreatePrizeDrawUi();
            Assert.AreEqual(1, ui.ParseDayLineInput("1 1", MaximumNumberOfOrders - 1).Count);
        }

        [TestMethod]
        public void MaximumTotalOrdersUpperBounds()
        {
            var ui = CreatePrizeDrawUi();
            Assert.IsNull(ui.ParseDayLineInput("1 1", MaximumNumberOfOrders));
        }
    }
}

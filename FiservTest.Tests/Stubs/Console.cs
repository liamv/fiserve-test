﻿using FiservTest.Ui;

namespace FiservTest.Tests.Stubs
{
    internal class Console : IConsole
    {
        public void Write(string value)
        {
        }

        public void WriteError(string value)
        {
        }

        public string Read()
        {
            return "";
        }
    }
}

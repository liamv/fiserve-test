﻿using System.Collections.Generic;
using FiservTest.Core;

namespace FiservTest.Tests.Stubs
{
    internal class PrizeDraw : IPrizeDraw
    {
        public void AddOrders(List<int> newOrders)
        {
        }

        public int CalculatePrize()
        {
            return 0;
        }

        public long GetTotalPrizes() => 0;
        public int GetDrawSize() => 0;
    }
}

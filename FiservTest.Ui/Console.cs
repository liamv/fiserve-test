﻿namespace FiservTest.Ui
{
    public class Console : IConsole
    {
        public Console()
        {
            System.Console.OutputEncoding = System.Text.Encoding.ASCII;
        }

        public void Write(string value)
        {
            System.Console.WriteLine(value);
        }

        public void WriteError(string value)
        {
            System.Console.Error.WriteLine("ERROR - " + value);
        }

        public string Read()
        {
            return System.Console.ReadLine();
        }
    }
}

﻿namespace FiservTest.Ui
{
    public interface IConsole
    {
        void Write(string value);
        void WriteError(string value);
        string Read();
    }
}

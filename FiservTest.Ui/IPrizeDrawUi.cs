﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FiservTest.Core;

namespace FiservTest.Ui
{
    public interface IPrizeDrawUi
    {
        void Start(IPrizeDraw prizeDraw);
        int? ParseNumberOfDaysInput(string input);
        List<int> ParseDayLineInput(string input, int totalNumberOfOrders = 0);
        int? ParseDayLineOrderCountInput(string input);
        List<int> ParseDayLineOrderValuesInput(List<string> inputValues);
    }
}

﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using FiservTest.Core;

namespace FiservTest.Ui
{
    public class PrizeDrawUi : IPrizeDrawUi
    {
        protected int MaximumNumberOfDays = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfDays"]);
        protected int MaximumOrderValue = int.Parse(ConfigurationManager.AppSettings["MaximumOrderValue"]);
        protected int MaximumNumberOfOrdersPerDay = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrdersPerDay"]);
        protected int MaximumNumberOfOrders = int.Parse(ConfigurationManager.AppSettings["MaximumNumberOfOrders"]);

        protected readonly IConsole Console;

        public PrizeDrawUi(IConsole console)
        {
            Console = console;
        }

        public void Start(IPrizeDraw prizeDraw)
        {
            int? numberOfDays = null;
            while (numberOfDays == null)
            {
                numberOfDays = ParseNumberOfDaysInput(Console.Read());
            }

            int totalNumberOfOrders = 0;

            for (int i = 0; i < numberOfDays; i++)
            {
                List<int> orders = null;
                while (orders == null)
                {
                    orders = ParseDayLineInput(Console.Read(), totalNumberOfOrders);
                }

                totalNumberOfOrders += orders.Count;
                prizeDraw.AddOrders(orders);
                prizeDraw.CalculatePrize();
            }

            Console.Write(prizeDraw.GetTotalPrizes().ToString());
            Console.Read();
        }


        public int? ParseNumberOfDaysInput(string input)
        {
            if (!int.TryParse(input, out int numberOfDays))
            {
                Console.WriteError("Invalid number, please try again");
                return null;
            }
            else if (numberOfDays < 1 || numberOfDays > MaximumNumberOfDays)
            {
                Console.WriteError($"Number of Days must be between 1 and {MaximumNumberOfDays}, please try again");
                return null;
            }

            return numberOfDays;
        }

        public List<int> ParseDayLineInput(string input, int totalNumberOfOrders = 0)
        {
            var inputValues = (input ?? "").Split(' ').ToList();
            var orderCount = ParseDayLineOrderCountInput(inputValues[0]);

            if (orderCount == null)
            {
                return null;
            }
            else if (totalNumberOfOrders + orderCount > MaximumNumberOfOrders)
            {
                Console.WriteError($"Maximum Total Number of Orders ({MaximumNumberOfOrders}) exceeded");
                return null;
            }
            else if (orderCount == 0 && inputValues.Count > 1)
            {
                Console.WriteError($"Order Count is zero, but ({inputValues.Count - 1}) values entered");
                return null;
            }

            var orders = ParseDayLineOrderValuesInput(inputValues.Skip(1).ToList());

            if (orders == null)
            {
                return null;
            }
            else if (orders.Count != orderCount)
            {
                Console.WriteError($"Order Count ({orderCount}) does not match number of entered values ({orders.Count})");
                return null;
            }

            return orders;
        }

        public int? ParseDayLineOrderCountInput(string input)
        {
            if (!int.TryParse(input, out var numberOfOrders))
            {
                Console.WriteError("Invalid Number of Orders entered");
                return null;
            }
            else if (numberOfOrders < 0 || numberOfOrders > MaximumNumberOfOrdersPerDay)
            {
                Console.WriteError($"Number of Orders must be between 0 and {MaximumNumberOfOrdersPerDay}");
                return null;
            }

            return numberOfOrders;
        }

        public List<int> ParseDayLineOrderValuesInput(List<string> inputValues)
        {
            var orders = new List<int>();

            foreach (var orderInput in inputValues)
            {
                if (!int.TryParse(orderInput, out var orderValue))
                {
                    Console.WriteError($"{orderInput} is not a valid Order Value");
                    return null;
                }
                else if (orderValue > MaximumOrderValue || orderValue < 1)
                {
                    Console.WriteError($"Order Value must be between 1 and {MaximumOrderValue}");
                    return null;
                }

                orders.Add(orderValue);
            }

            return orders;
        }
    }
}

﻿using FiservTest.Core;

namespace FiservTest.Ui
{
    class Program
    {
        static void Main(string[] args)
        {
            IPrizeDrawUi ui = new PrizeDrawUi(new Console());
            ui.Start(new PrizeDraw());
        }
    }
}